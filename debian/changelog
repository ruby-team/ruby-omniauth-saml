ruby-omniauth-saml (2.2.1-1) unstable; urgency=medium

  * Update minimum version of ruby-saml to 1.12
  * New upstream version 2.2.1
  * Bump Standards-Version to 4.7.0 (no changes needed)
  * Update minimum version of ruby-saml to 1.17~

 -- Pirate Praveen <praveen@debian.org>  Fri, 11 Oct 2024 18:40:27 +0530

ruby-omniauth-saml (2.1.0-2) unstable; urgency=medium

  * Reupload to unstable

 -- Pirate Praveen <praveen@debian.org>  Sun, 23 Oct 2022 22:51:15 +0530

ruby-omniauth-saml (2.1.0-1) experimental; urgency=medium

  * Team Upload.

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on ruby-omniauth and ruby-saml.
    + ruby-omniauth-saml: Drop versioned constraint on ruby-omniauth and
      ruby-saml in Depends.

  [ Mohammed Bilal ]
  * New upstream version 2.1.0 (Closes: #999725)
  * clean coverage directory
  * Bump debhelper compatibility level to 13
  * Bump Standards-Version to 4.6.1 (no changes needed)
  * Switch to ruby:Depends for ruby dependencies
  * Update min version of omniauth

 -- Mohammed Bilal <mdbilal@disroot.org>  Sun, 15 May 2022 13:09:11 +0530

ruby-omniauth-saml (1.10.0-1) unstable; urgency=medium

  * New upstream version 1.10.0
  * Bump debhelper compatibility level to 11
  * Bump Standards-Version to 4.1.3 (no changes needed)
  * Use salsa.debian.org in Vcs-* fields

 -- Pirate Praveen <praveen@debian.org>  Tue, 27 Mar 2018 16:10:48 +0530

ruby-omniauth-saml (1.7.0-1) unstable; urgency=medium

  * New upstream release
  * Bump minimum version of ruby-saml to 1.4

 -- Pirate Praveen <praveen@debian.org>  Sun, 30 Oct 2016 16:19:31 +0530

ruby-omniauth-saml (1.6.0-1) unstable; urgency=medium

  * New upstream minor release

 -- Pirate Praveen <praveen@debian.org>  Sun, 10 Jul 2016 20:31:25 +0530

ruby-omniauth-saml (1.5.0-1) unstable; urgency=medium

  * New upstream release
  * Add myself to uploaders

 -- Pirate Praveen <praveen@debian.org>  Wed, 16 Mar 2016 20:44:36 +0530

ruby-omniauth-saml (1.4.1-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Remove patch spec-fix (already applied)
  * Update watch file to use github tarballs
  * Check dependencies in gemspec
  * Set minimum version of ruby-saml to 1.0.0

 -- Pirate Praveen <praveen@debian.org>  Thu, 24 Sep 2015 20:20:25 +0530

ruby-omniauth-saml (1.3.1-1) unstable; urgency=medium

  * Initial release (Closes: #791701)

 -- Balasankar C <balasankarc@autistici.org>  Tue, 07 Jul 2015 22:49:33 +0530
